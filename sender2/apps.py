from django.apps import AppConfig
from signals.signals import my_signal1, my_signal2
from signals.receivers import receiver1, receiver2


class Sender2Config(AppConfig):
    name = 'sender2'

    def ready(self):

        # print(my_signal2.receivers)
        my_signal1.connect(receiver1)
        my_signal2.connect(receiver2)
        # print(my_signal2.receivers)
      
