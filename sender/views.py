from django.shortcuts import render
from django.http import HttpResponse
from signals.signals import my_signal1, my_signal2


items = 3


def index(request):
    
    # if items == 3:
        # my_signal.send(sender=None)

    # my_signal1.send(sender=None)
    my_signal2.send(sender=None)
        
    return HttpResponse("Signal Tested.")

