from django.apps import AppConfig
from signals.signals import my_signal1, my_signal2 
from signals.receivers import receiver1, receiver2


class SenderConfig(AppConfig):
    name = 'sender'


    def ready(self):

        my_signal1.connect(receiver1)
        my_signal2.connect(receiver2)
   